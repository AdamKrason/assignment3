INSERT INTO character (name, alias, gender, picture)
VALUES ('Anakin Skywalker', 'Darth Vader', 'male',
        'https://static.wikia.nocookie.net/starwars/images/5/59/Anakin_skywalker.jpg/revision/latest/scale-to-width-down/275?cb=20080428120001&path-prefix=no');
INSERT INTO character (name, alias, gender, picture)
VALUES ('Sheev Palpatine', 'The Emperor', 'male',
        'https://static.wikia.nocookie.net/starwars/images/d/d8/Emperor_Sidious.png/revision/latest?cb=20130620100935');
INSERT INTO character (name, alias, gender, picture)
VALUES ('Padme Amidala', 'Padme', 'female',
        'https://static.wikia.nocookie.net/starwars/images/b/b2/Padmegreenscrshot.jpg/revision/latest?cb=20100423143631');
INSERT INTO character (name, alias, gender, picture)
VALUES ('Peter Parker', 'Spider-Man', 'male',
        'https://substackcdn.com/image/fetch/w_1456,c_limit,f_webp,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F7db254f7-79aa-4a17-aad4-ca6515c7cbc8_1200x675.jpeg');
INSERT INTO character (name, alias, gender, picture)
VALUES ('Norman Osborn', 'Green Goblin', 'male',
        'https://www.writeups.org/wp-content/uploads/Green-Goblin-Spider-Man-movie-Osborn-Willem-Dafoe-l.jpg');
INSERT INTO character (name, alias, gender, picture)
VALUES ('Otto Otavius', 'Doctor Octopus', 'male',
        'https://static.wikia.nocookie.net/spiderman-films/images/3/37/Doctor_Octopus.jpg/revision/latest/scale-to-width-down/1200?cb=20130120100202');
