package no.accelerate.assignment3.repositories;

import no.accelerate.assignment3.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;


public interface CharacterRepository extends JpaRepository<Character, Integer> {

    @Query("Select c from Character c where c.name Like %?1%")
    Set<Character> findAllByNme(String name);
}
