package no.accelerate.assignment3.services.character;

import no.accelerate.assignment3.models.Character;
import no.accelerate.assignment3.models.Movie;
import no.accelerate.assignment3.services.CrudService;

import java.util.Collection;

public interface CharacterService extends CrudService<Character, Integer> {

    Collection<Character> findAll();
}
