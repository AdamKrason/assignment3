package no.accelerate.assignment3.models;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 100, nullable = false)
    private String title;
    @Column(length = 50, nullable = false)
    private String genre;
    @Column(length = 4, nullable = false)
    private int releaseYear;
    @Column(length = 50)
    private String director;
    @Column(length = 500)
    private String picture;
    @Column(length = 500)
    private String trailer;
}
